import React from 'react';
import PropTypes from 'prop-types';
import { colors } from '../style/settings';

const styles = {
  button: {
    fontSize: 13,
    letterSpacing: 3.7,
    textTransform: 'uppercase',
    borderRadius: '30px',
    backgroundColor: colors.primary,
    color: colors.white,
    border: 'none',
    fontWeight: 500,
    padding: '17px 43px',
  },
};

const RoundedButton = props => <button {...props}>{props.children}</button>;
RoundedButton.propTypes = {
  children: PropTypes.node.isRequired,
  style: PropTypes.instanceOf(Object),
};
RoundedButton.defaultProps = {
  style: styles.button,
};

export { RoundedButton };
