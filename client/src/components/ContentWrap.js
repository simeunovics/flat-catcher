import React from 'react';
import PropTypes from 'prop-types';

const style = {
  display: 'flex',
  minHeight: 'calc(100vh - 115px)',
  flexDirection: 'column',
};
const ContentWrap = ({ children }) => <main style={style}>{children}</main>;
ContentWrap.propTypes = {
  children: PropTypes.node.isRequired,
};

export { ContentWrap };
