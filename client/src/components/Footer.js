import React from 'react';

const styles = {
  footer: {
    textAlign: 'center',
    fontSize: 13,
    opacity: 0.6,
    width: '100%',
    padding: '50px 0',
  },
};

const Footer = () => (
  <footer style={styles.footer}>
    <span role="img" aria-label="Poop">
        💩
    </span>
      &nbsp;proudly made by&nbsp;
    <span aria-label="Monkey" role="img">
        🙉
    </span>
  </footer>
);

export { Footer };
