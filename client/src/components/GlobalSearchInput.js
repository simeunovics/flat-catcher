import React from 'react';
import PropTypes from 'prop-types';
import { RoundedButton } from './Buttons';
import { colors } from '../style/settings';

const styles = {
  searchInput: {
    container: {
      display: 'flex',
      flex: 1,
      alignItems: 'center',
      border: `1px solid ${colors.grayLighter}`,
      backgroundColor: colors.white,
      fontSize: 18,
      paddingLeft: 20,
      paddingTop: 0,
      paddingBottom: 0,
      paddingRight: 10,
      minHeight: 45,
    },
    input: {
      outline: 'none',
      border: 'none',
      marginLeft: 20,
      minHeight: 70,
      flex: 2,
    },
  },
  searchIcon: {
    color: colors.grayLight,
  },
  cancelButton: {
    color: colors.grayLight,
    paddingRight: 15,
    cursor: 'pointer',
  },
};

const GlobalSearchInput = ({
  onKeyDown,
  getInputProps,
  clearSelection,
  cancelButtonVisible,
  fetchingSuggestions,
  onSearchButtonClick,
}) => (
  <div style={styles.searchInput.container}>
    <i
      style={styles.searchIcon}
      aria-hidden="true"
      className={fetchingSuggestions ? 'spin icon_loading' : 'icon_search'}
    />
    <input
      {...getInputProps({
        onKeyDown,
        style: styles.searchInput.input,
        placeholder: 'Try: "Kragujevac"',
      })}
    />
    {cancelButtonVisible && (
      <i
        style={styles.cancelButton}
        onClick={clearSelection}
        aria-hidden="true"
        className="icon_close"
      />
    )}
    <RoundedButton onClick={onSearchButtonClick}>Search</RoundedButton>
  </div>
);

GlobalSearchInput.propTypes = {
  onKeyDown: PropTypes.func.isRequired,
  getInputProps: PropTypes.func.isRequired,
  clearSelection: PropTypes.func.isRequired,
  cancelButtonVisible: PropTypes.bool.isRequired,
  fetchingSuggestions: PropTypes.bool.isRequired,
  onSearchButtonClick: PropTypes.func.isRequired,
};

export { GlobalSearchInput };
