import React, { Fragment } from 'react';
import { colors } from '../style/settings';

const styles = {
  header: {
    textAlign: 'center',
    letterSpacing: 'normal',
    marginTop: 130,
  },
  main: {
    margin: 0,
    paddingTop: 3,
    paddingBotton: 3,
    fontSize: 60,
    fontWeight: 300,
  },
  sub: {
    color: colors.grayLight,
    padding: 0,
    margin: 0,
    fontSize: 15,
    fontWeight: 300,
  },
};
const Header = () => (
  <Fragment>
    <header style={styles.header}>
      <h1 style={styles.main}>FlatCatcher</h1>
      <h2 style={styles.sub}>Housing scout for lazy people</h2>
    </header>
  </Fragment>
);

export { Header };
