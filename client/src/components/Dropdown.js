import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { colors } from '../style/settings';

const styles = {
  container: {
    width: '100%',
    position: 'relative',
    fontSize: 16,
    fontWeight: 400,
    backgroundColor: colors.grayLightest,
    color: colors.gray,
    border: `1px solid ${colors.grayLighter}`,
  },
  option: {
    cursor: 'pointer',
    fontSize: 16,
    fontWeight: 400,
    padding: '15px 20px',
    color: colors.gray,
    backgroundColor: colors.white,
    divider: {
      height: 1,
      borderTop: `1px solid ${colors.grayLighter}`,
      marginTop: 1,
      marginBottom: 1,
    },
    active: {
      cursor: 'pointer',
      fontSize: 16,
      fontWeight: 400,
      padding: '15px 20px',
      backgroundColor: colors.grayLightest,
      color: colors.dark,
    },
  },
  delimiter: {
    color: colors.gray,
    display: 'none',
  },
  displayedValue: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  drawler: {
    backgroundColor: colors.grayLightest,
    border: `1px solid ${colors.grayLighter}`,
    width: '100%',
    position: 'absolute',
    top: 49,
    left: -1,
  },
};

class Dropdown extends Component {
  static propTypes = {
    text: PropTypes.string.isRequired,
    open: PropTypes.bool,
    children: PropTypes.node.isRequired,
  };
  static defaultProps = {
    open: false,
  };

  state = {
    open: this.props.open,
  };

  componentDidMount() {
    window.addEventListener('click', this.closeOnOffScreenClick);
  }

  componentWillUnmount() {
    window.removeEventListener('click', this.closeOnOffScreenClick);
  }
  dropdown = null;

  closeOnOffScreenClick = ({ target }) =>
    !this.dropdown.contains(target) && this.setState({ open: false });

  toggle = () => this.setState({ open: !this.state.open });

  render() {
    return (
      <div style={styles.container}>
        <div
          ref={(elem) => {
            this.dropdown = elem;
          }}
          style={{ ...styles.option, ...styles.displayedValue }}
          onClick={this.toggle}
          role="button"
          tabIndex="0"
          onKeyPress={this.toggle}
        >
          <span> {this.props.text} </span>
          <span>
            <i className={this.state.open ? 'arrow_carrot-up' : 'arrow_carrot-down'} />
          </span>
        </div>
        {this.state.open && (
          <div
            className="dropdown"
            onClick={this.toggle}
            onKeyPress={this.toggle}
            role="button"
            tabIndex="0"
            style={styles.drawler}
          >
            {this.props.children({
              optionStyle: styles.option,
            })}
          </div>
        )}
      </div>
    );
  }
}

export { Dropdown };
