import React from 'react';
import PropTypes from 'prop-types';
import { colors } from '../style/settings';

const styles = {
  container: {},
  imageHolder: {
    borderLeft: `1px solid ${colors.grayLighter}`,
    borderTop: `1px solid ${colors.grayLighter}`,
    borderRight: `1px solid ${colors.grayLighter}`,
    height: 360,
    textAlign: 'center',
    width: '100%',
    alignItems: 'center',
    display: 'flex',
    backgroundColor: colors.grayLighter,
    backgroundSize: 'cover',
    backgroundPosition: 'center center',
  },
  body: {
    padding: 20,
    width: 'calc(100% - 40px)',
    backgroundColor: colors.white,
    borderLeft: `1px solid ${colors.grayLighter}`,
    borderRight: `1px solid ${colors.grayLighter}`,
    borderBottom: `1px solid ${colors.grayLighter}`,
  },
  title: {
    padding: 0,
    margin: 0,
    fontSize: 19,
    display: 'block',
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
  },
  price: {
    padding: '1px 5px',
    fontSize: 30,
    color: colors.dark,
    display: 'inline-block',
    fontWeight: '300',
  },
  divider: {
    borderTop: `1px dashed ${colors.grayLighter}`,
    margin: '0 auto',
  },
  details: {
    featureBoxes: {
      container: {
        marginTop: 20,
      },
      box: {
        display: 'inline-block',
        paddingRight: 20,
        fontSize: 14,
        icon: {
          fontSize: 15,
          color: colors.gray,
        },
        desc: {
          verticalAlign: 'top',
          paddingLeft: 5,
        },
      },
    },
  },
};

const FlatCard = ({
  image,
  altImage,
  title,
  link,
  leaseType,
  price,
  squareMeters,
  numberOfRooms,
}) => (
  <div style={styles.container}>
    <div
      style={{
        ...styles.imageHolder,
        backgroundImage: `url(${image}), url(${altImage})`,
      }}
    />
    <div style={styles.body}>
      <a style={styles.title} title={title} target="_blank" rel="noopener noreferrer" href={link}>
        {title}
      </a>
      <p
        style={{
          ...styles.price,
          backgroundColor: leaseType === 'rent' ? colors.secondary : colors.secondaryAlt,
        }}
      >
        {parseInt(price, 10).toLocaleString()}&euro;{' '}
      </p>
      <div style={styles.divider} />
      <section style={styles.details.featureBoxes.container}>
        <div title="Appartment size" style={styles.details.featureBoxes.box}>
          <i style={styles.details.featureBoxes.box.icon} className="dripicons-duplicate" />
          <span style={styles.details.featureBoxes.box.desc}>
            {squareMeters}m<sup>2</sup>
          </span>
        </div>
        <div title="Number of rooms" style={styles.details.featureBoxes.box}>
          <i style={styles.details.featureBoxes.box.icon} className="dripicons-view-thumb" />
          <span style={styles.details.featureBoxes.box.desc}>{numberOfRooms}</span>
        </div>
        {leaseType === 'sell' && (
          <div title="Price per square meter" style={styles.details.featureBoxes.box}>
            <i style={styles.details.featureBoxes.box.icon} className="dripicons-graph-pie" />
            <span style={styles.details.featureBoxes.box.desc}>
              {Math.round(parseInt(price, 10) / parseInt(squareMeters, 10)).toLocaleString()}
              <sup>&euro;</sup> / m<sup>2</sup>
            </span>
          </div>
        )}
      </section>
    </div>
  </div>
);
FlatCard.propTypes = {
  title: PropTypes.string.isRequired,
  price: PropTypes.string.isRequired,
  image: PropTypes.string.isRequired,
  link: PropTypes.string.isRequired,
  altImage: PropTypes.string.isRequired,
  squareMeters: PropTypes.string.isRequired,
  numberOfRooms: PropTypes.string.isRequired,
  leaseType: PropTypes.string.isRequired,
};

export { FlatCard };
