import React from 'react';
import PropTypes from 'prop-types';
import { colors } from '../style/settings';

const styles = {
  leasePicker: {
    container: {
      flex: 1,
      display: 'flex',
      alignItems: 'center',
      marginBottom: 20,
    },
    text: {
      fontSize: 18,
      paddingRight: 10,
    },
  },
  rentButton: {
    width: 100,
    textTransform: 'uppercase',
    fontSize: 12,
    padding: '10px 20px',
    backgroundColor: colors.secondary,
    color: colors.secondaryDark,
    border: 'none',
    letterSpacing: 4.3,
    fontWeight: 600,
  },
  buyButton: {
    width: 100,
    textTransform: 'uppercase',
    border: 'none',
    fontSize: 12,
    padding: '10px 20px',
    backgroundColor: colors.secondaryAlt,
    color: colors.secondaryAltDark,
    letterSpacing: 4.3,
    fontWeight: 600,
  },
};

const LeaseTypeButtons = ({ setLeaseType, leaseType }) => (
  <div style={styles.leasePicker.container}>
    <span style={styles.leasePicker.text}>I want to</span>{' '}
    <button
      onClick={() => leaseType !== 'rent' && setLeaseType('rent')}
      style={{
        ...styles.rentButton,
        fontWeight: leaseType === 'rent' ? 800 : 300,
      }}
    >
      Rent
    </button>
    <button
      onClick={() => leaseType !== 'sell' && setLeaseType('sell')}
      style={{
        ...styles.buyButton,
        fontWeight: leaseType === 'sell' ? 800 : 300,
      }}
    >
      Buy
    </button>
  </div>
);
LeaseTypeButtons.propTypes = {
  setLeaseType: PropTypes.func.isRequired,
  leaseType: PropTypes.string.isRequired,
};

export { LeaseTypeButtons };
