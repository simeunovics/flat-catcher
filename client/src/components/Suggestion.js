import React from 'react';
import PropTypes from 'prop-types';
import { colors } from '../style/settings';

const highilihgtWords = (text, keywords) => {
  // User input: beog jurija gagarin
  // regex /(beog)|(jurija)|(gagarin)/gi
  const regex = (keywords)
    .trim()
    .split(' ')
    .map(keyword => `(${keyword})`)
    .join('|');

  return text
    .trim()
    .split(new RegExp(regex, 'ig'))
    .map((word = '', index) =>
      (keywords.toLowerCase().includes(word.toLowerCase()) ? (
        <strong key={`${word}-${index}`}>{word}</strong> // eslint-disable-line
      ) : (
        word
      )));
};

const styles = {
  suggestion: {
    markerIcon: {
      color: colors.grayLight,
    },
    item: {
      backgroundColor: colors.white,
      fontSize: 18,
      paddingLeft: 20,
      paddingTop: 20,
      paddingBottom: 20,
      width: '100%',
      border: 'none',
      borderLeft: `1px solid ${colors.grayLighter}`,
      borderRight: `1px solid ${colors.grayLighter}`,
      borderBottom: `1px solid ${colors.grayLighter}`,
      display: 'flex',
      alignItems: 'center',
      textAlign: 'left',
      text: {
        paddingLeft: 20,
        flex: 2,
        color: colors.dark,
      },
      badge: {
        fontSize: 12,
        fontWeight: 600,
        padding: '5px 15px',
        backgroundColor: colors.secondary,
        color: colors.secondaryDark,
      },
    },
  },
};

const Suggestion = ({
  getItemProps, text, valueUserTyped, highlightedIndex, index,
}) => (
  <button
    {...getItemProps({
      item: text,
      style: {
        ...styles.suggestion.item,
        backgroundColor: highlightedIndex === index ? colors.grayLightest : colors.white,
      },
    })}
  >
    <i style={styles.suggestion.markerIcon} className="icon_pin_alt" />
    <span style={styles.suggestion.item.text}>
      {Boolean(text) && highilihgtWords(text, valueUserTyped)}
    </span>
  </button>
);
Suggestion.propTypes = {
  text: PropTypes.string.isRequired,
  valueUserTyped: PropTypes.string.isRequired,
  getItemProps: PropTypes.func.isRequired,
  highlightedIndex: PropTypes.number,
  index: PropTypes.number,
};
Suggestion.defaultProps = {
  index: 0,
  highlightedIndex: 0,
};

export { Suggestion };
