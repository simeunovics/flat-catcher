import React from 'react';
import { colors } from '../style/settings';

const styles = {
  nav: {
    flex: 1,
    height: 80,
    display: 'flex',
    paddingLeft: 28,
    alignItems: 'center',
  },
  icon: {
    fontSize: 20,
    paddingTop: 3,
    color: colors.primary,
  },
  link: {
    flex: 1,
    display: 'flex',
    alignItems: 'center',
    color: colors.gray,
  },
  navText: {
    textTransform: 'uppercase',
    fontSize: 13,
    letterSpacing: 3.9,
  },
};

const Nav = () => (
  <nav style={styles.nav}>
    <a href="/" style={styles.link}>
      <i style={styles.icon} className="icon dripicons-home" />{' '}
      <span
        style={{
            ...styles.navText,
            fontWeight: 500,
            paddingLeft: 10,
            color: colors.dark,
          }}
      >
          Flat
      </span>
      <span style={styles.navText}>catcher</span>
    </a>
  </nav>
);

export { Nav };
