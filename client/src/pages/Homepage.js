import React from 'react';
import { GlobalSearchForm, FlatList, SearchTools } from '../containers';

const Homepage = () => (
    <section>
        <GlobalSearchForm />
        <SearchTools />
        <FlatList />
    </section>
);

export { Homepage };
