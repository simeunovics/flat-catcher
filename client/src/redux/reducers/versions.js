import * as Actions from '../actions/ActionTypes';

const initState = {
    rentBatchId: null,
    sellBatchId: null,
};

const versions = (state = initState, action) => {
    switch (action.type) {
        case Actions.SET_RENT_BATCH_ID:
            return {
                ...state,
                rentBatchId: action.payload,
            };
        case Actions.SET_SELL_BATCH_ID:
            return {
                ...state,
                sellBatchId: action.payload,
            };
        default:
            return state;
    }
};

export { versions };
