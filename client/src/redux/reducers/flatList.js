import * as Actions from '../actions/ActionTypes';
const initState = {
    list: [],
    currentPage: 0,
    perPage: 100,
    fetchingFlats: false,
};

export const flatList = (state = initState, action) => {
    switch (action.type) {
        case Actions.SET_FLAT_LIST:
            return {
                ...state,
                list: action.payload,
            };
        case Actions.SET_FLAT_LIST_CURRENT_PAGE:
            return {
                ...state,
                currentPage: action.payload,
            };
        case Actions.SET_FLAT_PER_PAGE:
            return {
                ...state,
                perPage: action.payload,
            };
        case Actions.SET_FETCHING_FLATS:
            return {
                ...state,
                fetchingFlats: action.payload,
            };
        default:
            return state;
    }
};
