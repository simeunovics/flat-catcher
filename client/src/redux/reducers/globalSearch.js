import * as Actions from '../actions/ActionTypes';

const initState = {
    searchQuery: '',
    leaseType: 'rent',
    sortField: 'createdAt',
    sortDirection: 'desc',
    minPrice: 0,
    maxPrice: 999999999,
    selectedKeywords: [],
    suggestionsList: [],
    fetchingSuggestions: false,
    numberOfRooms: null,
};

const globalSearch = (state = initState, action) => {
    switch (action.type) {
        case Actions.SET_SEARCH_QUERY:
            return {
                ...state,
                searchQuery: action.payload,
            };
        case Actions.SET_LEASE_TYPE:
            return {
                ...state,
                leaseType: action.payload,
            };
        case Actions.SET_SORT_FIELD:
            return {
                ...state,
                sortField: action.payload,
            };
        case Actions.SET_SORT_DIRECTION:
            return {
                ...state,
                sortDirection: action.payload,
            };
        case Actions.SET_MIN_PRICE:
            return {
                ...state,
                minPrice: action.payload,
            };
        case Actions.SET_MAX_PRICE:
            return {
                ...state,
                maxPrice: action.payload,
            };
        case Actions.SET_SELECTED_KEYWORDS:
            return {
                ...state,
                selectedKeywords: action.payload,
            };
        case Actions.SET_SUGGESTIONS_LIST:
            return {
                ...state,
                suggestionsList: action.payload,
            };
        case Actions.SET_FETCHING_SUGGESTIONS_IN_PROGRESS:
            return {
                ...state,
                fetchingSuggestions: action.payload,
            };
        case Actions.SET_NUMBER_OF_ROOMS:
            return {
                ...state,
                numberOfRooms: action.payload,
            };
        default:
            return state;
    }
};

export { globalSearch };
