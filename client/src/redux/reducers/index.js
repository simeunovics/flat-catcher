import { combineReducers } from 'redux';
import { globalSearch } from './globalSearch';
import { flatList } from './flatList';
import { versions } from './versions';

export const reducers = combineReducers({
    globalSearch,
    flatList,
    versions,
});
