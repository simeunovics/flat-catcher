import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import { reducers } from '../reducers';
import { Cache } from '../../services/Cache';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
    reducers,
    Cache.get('redux-store'),
    composeEnhancers(applyMiddleware(thunk)),
);

if (process.env.NODE_ENV !== 'production') {
    console.info('Store content will be persisted in local storage!');
    store.subscribe(() => Cache.set('redux-store', store.getState()));
}
export { store };
