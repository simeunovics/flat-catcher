import * as Actions from './ActionTypes';
import { Api } from '../../services/Api';

export const setFlatList = list => ({
    type: Actions.SET_FLAT_LIST,
    payload: list,
});
export const setCurrentPage = page => ({
    type: Actions.SET_FLAT_LIST_CURRENT_PAGE,
    payload: page,
});
export const setFetchingFlats = flag => ({
    type: Actions.SET_FETCHING_FLATS,
    payload: flag,
});

export const resetFlatList = () => (dispatch, getState) => {
    dispatch(setFlatList([]));
    dispatch(setCurrentPage(0));
};

export const loadMoreFlats = () => (dispatch, getState) => {
    const state = getState();
    const { currentPage } = state.flatList;

    dispatch(setCurrentPage(+currentPage + 1));
    dispatch(fetchListOfFlats());
};

export const fetchListOfFlats = () => (dispatch, getState) => {
    const state = getState();
    const { currentPage, perPage, list } = state.flatList;
    const { rentBatchId, sellBatchId } = state.versions;
    const {
        leaseType,
        selectedKeywords,
        sortField,
        sortDirection,
        minPrice,
        maxPrice,
        numberOfRooms,
    } = state.globalSearch;
    const batchId = leaseType === 'rent' ? rentBatchId : sellBatchId;

    if (!selectedKeywords.length) {
        return;
    }

    const endpoint = leaseType === 'rent' ? 'flatsForRent' : 'flatsForSell';
    const skip = currentPage * perPage;
    const locations = selectedKeywords.join(',');
    const filters = `locations:"${locations}" skip:${skip} limit:${perPage} sortField:"${sortField}" sortDirection:"${sortDirection}" minPrice:${minPrice} maxPrice:${maxPrice} numberOfRooms:${numberOfRooms} batchId:"${batchId}"`;
    const query = `{${endpoint}(${filters}){extId title numberOfRooms squareMeters shortDescription price images{url}link places{text}}}`;

    dispatch(setFetchingFlats(true));
    Api.get(query).then(({ data }) => {
        const flats =
            leaseType === 'rent' ? data.flatsForRent : data.flatsForSell;
        dispatch(setFlatList([...list, ...flats]));
        dispatch(setFetchingFlats(false));
    });
};
