import * as Actions from './ActionTypes';
import { Api } from '../../services/Api';

export const setRentBatchId = batchId => ({
    type: Actions.SET_RENT_BATCH_ID,
    payload: batchId,
});
export const setSellBatchId = batchId => ({
    type: Actions.SET_SELL_BATCH_ID,
    payload: batchId,
});

export const fetchLatestBatchId = () => (dispatch, getState) => {
    Api.getLatestBatchVersion().then(({ rentBatchId, sellBatchId }) => {
        dispatch(setRentBatchId(rentBatchId));
        dispatch(setSellBatchId(sellBatchId));
    });
};
