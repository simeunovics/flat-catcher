import * as Actions from './ActionTypes';
import { Api } from '../../services/Api';

export const setLeaseType = leaseType => ({
    type: Actions.SET_LEASE_TYPE,
    payload: leaseType,
});
export const setSelectedKeywords = keywords => ({
    type: Actions.SET_SELECTED_KEYWORDS,
    payload: keywords,
});
export const setGlobalSearchQuery = query => ({
    type: Actions.SET_SEARCH_QUERY,
    payload: query,
});
export const setSuggestionList = suggestions => ({
    type: Actions.SET_SUGGESTIONS_LIST,
    payload: suggestions,
});
export const setSortDirection = direction => ({
    type: Actions.SET_SORT_DIRECTION,
    payload: direction,
});
export const setSortField = field => ({
    type: Actions.SET_SORT_FIELD,
    payload: field,
});
export const setMinPrice = price => ({
    type: Actions.SET_MIN_PRICE,
    payload: price,
});
export const setMaxPrice = price => ({
    type: Actions.SET_MAX_PRICE,
    payload: price,
});
export const setFetchingSuggestionsInProgress = flag => ({
    type: Actions.SET_FETCHING_SUGGESTIONS_IN_PROGRESS,
    payload: flag,
});
export const setNumberOfRooms = numberOfRooms => ({
    type: Actions.SET_NUMBER_OF_ROOMS,
    payload: numberOfRooms,
});

export const clearSuggestionList = () => setSuggestionList([]);

export const selectKeyword = keyword => (dispatch, getState) => {
    const state = getState();
    const selectedKeywords = state.globalSearch.selectedKeywords.slice();

    selectedKeywords.push(keyword);
    dispatch(setSelectedKeywords(selectedKeywords));
};
export const removeKeyword = keywordToRemove => (dispatch, getState) => {
    const state = getState();
    const selectedKeywords = state.globalSearch.selectedKeywords.filter(
        storedKeyword => storedKeyword !== keywordToRemove,
    );

    dispatch(setSelectedKeywords(selectedKeywords));
};

export const fetchSuggestions = () => (dispatch, getState) => {
    const state = getState();
    const { searchQuery, leaseType } = state.globalSearch;
    const { rentBatchId, sellBatchId } = state.versions;
    const batchId = leaseType === 'rent' ? rentBatchId : sellBatchId;
    dispatch(setFetchingSuggestionsInProgress(true));

    Api.get(
        `{locationAutocomplete(query: "${searchQuery}", leaseType:"${leaseType}" batchId:"${batchId}") { text }}`,
    ).then(({ data }) => {
        dispatch(setSuggestionList(data.locationAutocomplete));
        dispatch(setFetchingSuggestionsInProgress(false));
    });
};
