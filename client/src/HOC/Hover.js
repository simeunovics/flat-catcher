import React, { Component } from 'react';

class Hover extends Component {
    state = {
        active: false,
    };

    on = () => this.setState({ active: true });
    off = () => this.setState({ active: false });

    render() {
        return (
            <span onMouseEnter={this.on} onMouseLeave={this.off}>
                {this.props.children({ hovered: this.state.active })}
            </span>
        );
    }
}

export { Hover };
