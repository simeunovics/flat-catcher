export const colors = {
    primary: '#1b96fe',
    dark: '#212328',
    gray: '#a6a7aa',
    grayLight: '#737477',
    grayLighter: '#f1f1f1',
    grayLightest: '#fdfeff',
    secondary: '#e3f8f1',
    secondaryDark: '#78b29c',
    secondaryAlt: '#fef7db',
    secondaryAltDark: '#a69471',
    white: '#fff',
};

export const fonts = {
    primary: 'Montserrat',
};
