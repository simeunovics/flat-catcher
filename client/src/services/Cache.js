const DEFAULT_KEY = '2wmthjqMc227Hh6smGqV';

class LocalStorageCache {
    constructor(serialize, deserialize, store, retrieve, clear) {
        this.serializer = { serialize, deserialize };
        this.storage = { store, retrieve, clear };
    }

    get = (key = DEFAULT_KEY) => {
        try {
            return (
                this.serializer.deserialize(this.storage.retrieve(key)) ||
                undefined
            );
        } catch (err) {
            console.error(err);
            return undefined;
        }
    };

    set = (key = DEFAULT_KEY, state) => {
        try {
            this.storage.store(key, this.serializer.serialize(state));
        } catch (err) {
            console.error(err);
        }
    };

    clear = () => this.storage.clear();
}

export const Cache = new LocalStorageCache(
    JSON.stringify,
    JSON.parse,
    (...args) => localStorage.setItem(...args),
    (...args) => localStorage.getItem(...args),
    (...args) => localStorage.clear(...args),
);
