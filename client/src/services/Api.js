const Api = new class {
    API_BASE_URL = '/api';
    API_EXPLORER_URL = '/api/explorer';

    getLatestBatchVersion = () => {
        return fetch(`${this.API_BASE_URL}/versions`, {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
        }).then(res => res.json());
    };

    get = query =>
        fetch(this.API_EXPLORER_URL, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ query }),
        }).then(res => res.json());
}();

export { Api };
