import { connect } from 'react-redux';
import { LeaseTypeButtons as Buttons } from '../components';
import { setLeaseType, resetFlatList, fetchListOfFlats } from '../redux/actions';

const mapStateToProps = ({ globalSearch }) => ({
  leaseType: globalSearch.leaseType,
});
const mapDispatchToProps = dispatch => ({
  setLeaseType: (leaseType) => {
    dispatch(setLeaseType(leaseType));
    dispatch(resetFlatList());
    dispatch(fetchListOfFlats());
  },
});
const LeaseTypeButtons = connect(
  mapStateToProps,
  mapDispatchToProps,
)(Buttons);

export { LeaseTypeButtons };
