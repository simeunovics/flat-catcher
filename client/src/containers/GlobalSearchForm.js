import React, { Component } from 'react';
import { connect } from 'react-redux';
import Downshift from 'downshift';
import _ from 'lodash';
import { Suggestion, GlobalSearchInput as Input } from '../components';
import { LeaseTypeButtons } from './LeaseTypeButtons';
import {
  fetchSuggestions,
  setGlobalSearchQuery,
  clearSuggestionList,
  setSelectedKeywords,
  fetchListOfFlats,
  resetFlatList,
  fetchLatestBatchId,
} from '../redux/actions';

const USER_IS_TYPING_DEBOUNCE_TIMEOUT = 500;
const FETCHING_SUGGESTIONS_FROM_API_TIMEOUT = 500;

const styles = {
  autocomplete: {
    marginTop: 120,
    maxWidth: 965,
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  suggestion: {
    container: {
      display: 'flex',
      flex: 1,
      flexDirection: 'column',
    },
  },
};

class Autocomplete extends Component {
  timeout = null;
  state = {
    userIsTyping: false,
  };

  updateUserIsTyping = () => {
    this.setState({ userIsTyping: true });

    this.timeout && clearTimeout(this.timeout);
    this.timeout = setTimeout(() => {
      this.setState({ userIsTyping: false });
    }, USER_IS_TYPING_DEBOUNCE_TIMEOUT);
  };

  showNoAvailableItemsNotification = () => {
    if (this.state.userIsTyping) {
      return false;
    }

    if (this.props.fetchingSuggestions) {
      return false;
    }

    if (this.props.inputValue.length === 0) {
      return false;
    }

    if (this.props.items.length !== 0) {
      return false;
    }

    return true;
  };

  componentDidMount() {
    this.props.fetchLatestBatchId();
  }

  componentDidCatch(e) {
    if (e.message === 'stateToSet is not a function') {
      console.error(e);
      return;
    }

    throw e;
  }

  render() {
    const {
      items, onChange, inputValue, onInputValueChange, fetchingSuggestions,
    } = this.props;

    return (
      <Downshift
        onChange={onChange}
        inputValue={inputValue}
        onInputValueChange={onInputValueChange}
        render={({
          isOpen,
          selectItem,
          getItemProps,
          selectedItem,
          getInputProps,
          clearSelection,
          highlightedIndex,
        }) => (
          <div style={styles.autocomplete}>
            <LeaseTypeButtons />
            <Input
              onKeyDown={(e) => {
                this.updateUserIsTyping();

                if (e.key !== 'Enter') return;

                if (highlightedIndex !== null) return;

                selectItem(inputValue);
              }}
              getInputProps={getInputProps}
              clearSelection={clearSelection}
              cancelButtonVisible={Boolean(inputValue)}
              fetchingSuggestions={fetchingSuggestions}
              onSearchButtonClick={() => selectItem(inputValue)}
            />
            {isOpen && (
              <div style={styles.suggestion.container}>
                {this.showNoAvailableItemsNotification() && (
                  <Suggestion
                    getItemProps={args => args}
                    valueUserTyped={inputValue}
                    text={`Sorry there are no suggestions for ${inputValue}`}
                  />
                )}
                {items.map((item, index) => (
                  <Suggestion
                    text={item.text}
                    valueUserTyped={inputValue}
                    key={index}
                    getItemProps={getItemProps}
                    highlightedIndex={highlightedIndex}
                    index={index}
                  />
                ))}
              </div>
            )}
          </div>
        )}
      />
    );
  }
}

const fetchSuggestionsFromApi = _.debounce((dispatch) => {
  dispatch(fetchSuggestions());
}, FETCHING_SUGGESTIONS_FROM_API_TIMEOUT);

const highlihgtCommonKeywords = ({ suggestionsList, searchQuery }) => {
  const set = {};
  suggestionsList.map(item =>
    item.text
      .split('/')
      .map(keyword => keyword.trim())
      .filter(keyword => keyword.toLowerCase().includes(searchQuery.toLowerCase()))
      .map(match => (set[match] = 1)));
  return Object.keys(set).map(key => ({ text: key }));
};
const mapStateToProps = ({ globalSearch }) => ({
  fetchingSuggestions: globalSearch.fetchingSuggestions,
  inputValue: globalSearch.searchQuery,
  items: [...highlihgtCommonKeywords(globalSearch), ...globalSearch.suggestionsList],
  selectedItem: globalSearch.selectedKeywords,
});

const mapDispatchToProps = dispatch => ({
  onChange: (keyword) => {
    if (_.isEmpty(keyword)) {
      dispatch(setGlobalSearchQuery(''));
      dispatch(setSelectedKeywords([]));
      dispatch(resetFlatList());

      return;
    }

    dispatch(setSelectedKeywords([keyword]));
    dispatch(resetFlatList());
    dispatch(fetchListOfFlats());
  },
  onInputValueChange: (value) => {
    dispatch(setGlobalSearchQuery(value));

    value ? fetchSuggestionsFromApi(dispatch) : dispatch(clearSuggestionList());
  },
  fetchLatestBatchId: batchId => dispatch(fetchLatestBatchId()),
});
const GlobalSearchForm = connect(
  mapStateToProps,
  mapDispatchToProps,
)(Autocomplete);
export { GlobalSearchForm };
