import React, { Component } from 'react';
import { Cache } from '../services/Cache';

const styles = {
  container: {
    position: 'fixed',
    bottom: 0,
    left: 0,
    opacity: 0.8,
  },
  button: {
    border: 'none',
    padding: '10px 20px',
    textTransform: 'uppercase',
  },
};

class DeveloperToolbar extends Component {
  clearLocalCache = () => {
    Cache.clear();
    window.location.reload();
  };

  render() {
    return (
      <nav style={styles.container}>
        <button title="Clear Cache" style={styles.button} onClick={this.clearLocalCache}>
          !
        </button>
      </nav>
    );
  }
}

export { DeveloperToolbar };
