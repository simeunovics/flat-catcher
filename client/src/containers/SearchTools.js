import React, { Component } from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';
import Slider, { createSliderWithTooltip } from 'rc-slider';
import 'rc-slider/assets/index.css';
import { Dropdown } from '../components';
import { Hover } from '../HOC';

import {
  setSortField,
  setSortDirection,
  fetchListOfFlats,
  resetFlatList,
  setMinPrice,
  setMaxPrice,
  setNumberOfRooms,
} from '../redux/actions';
import { colors } from '../style/settings';

const styles = {
  container: {
    maxWidth: 960,
    marginTop: 20,
    marginLeft: 'auto',
    marginRight: 'auto',
    display: 'grid',
    gridTemplateColumns: '200px 200px auto 15px',
    gridGap: 30,
  },
  slider: {
    display: 'flex',
    height: '100%',
    alignItems: 'center',
  },
  loadMore: {
    display: 'flex',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'end',
    fontSize: 24,
    icon: {
      color: colors.gray,
      cursor: 'pointer',
    },
  },
  input: {
    fontSize: 16,
    fontWeight: 400,
    padding: '15px 20px',
    backgroundColor: colors.grayLightest,
    color: colors.gray,
    border: `1px solid ${colors.grayLighter}`,
    width: '50%',
  },
};

const Range = createSliderWithTooltip(Slider.Range);

class Main extends Component {
  state = {
    maxRange: this.props.leaseType === 'rent' ? 1000 : 500000,
    rangeStep: this.props.leaseType === 'rent' ? 10 : 1000,
  };
  humarReadableStrings = {
    createdAt: {
      asc: 'Oldest',
      desc: 'Newest',
    },
    price: {
      asc: 'Lowest Price',
      desc: 'Highest Price',
    },
  };
  extendMaxRange = () => this.setState({ maxRange: this.state.maxRange * 2 });

  label = (field, direction) => this.humarReadableStrings[field][direction];

  render() {
    const {
      field,
      direction,
      changeSort,
      changePrice,
      searchToolsVisible,
      numberOfRooms,
      setNumberOfRooms,
    } = this.props;

    if (!searchToolsVisible) {
      return '';
    }

    const options = [
      {
        text: this.label('createdAt', 'desc'),
        callback: () => changeSort('createdAt', 'desc'),
      },
      {
        text: this.label('createdAt', 'asc'),
        callback: () => changeSort('createdAt', 'asc'),
      },
      {
        text: null,
      },
      {
        text: this.label('price', 'desc'),
        callback: () => changeSort('price', 'desc'),
      },
      {
        text: this.label('price', 'asc'),
        callback: () => changeSort('price', 'asc'),
      },
    ];

    const numberOfRoomsOptions = [
      {
        text: '1',
        callback: () => setNumberOfRooms(1),
      },
      {
        text: '2',
        callback: () => setNumberOfRooms(2),
      },
      {
        text: '3',
        callback: () => setNumberOfRooms(3),
      },
      {
        text: '4',
        callback: () => setNumberOfRooms(4),
      },
      {
        text: '5',
        callback: () => setNumberOfRooms(5),
      },
      {
        text: '5+',
        callback: () => setNumberOfRooms(6),
      },
      {
        text: null,
      },
      {
        text: 'Any',
        callback: () => setNumberOfRooms(null),
      },
    ];

    return (
      <div style={styles.container}>
        <div>
          <Dropdown text={this.label(field, direction)}>
            {({ optionStyle }) =>
              options.map(({ text, callback }, index) => {
                if (text === null) {
                  return <div key={`divider_${index}`} style={optionStyle.divider} />;
                }

                return (
                  <Hover key={text}>
                    {({ hovered }) => (
                      <div onClick={callback} style={hovered ? optionStyle.active : optionStyle}>
                        {text}
                      </div>
                    )}
                  </Hover>
                );
              })
            }
          </Dropdown>
        </div>
        <div>
          <Dropdown text={`Rooms: ${numberOfRooms || 'Any'}`}>
            {({ optionStyle }) =>
              numberOfRoomsOptions.map(({ text, callback }, index) => {
                if (text === null) {
                  return <div key={`divider_${index}`} style={optionStyle.divider} />;
                }

                return (
                  <Hover key={text}>
                    {({ hovered }) => (
                      <div onClick={callback} style={hovered ? optionStyle.active : optionStyle}>
                        {text}
                      </div>
                    )}
                  </Hover>
                );
              })
            }
          </Dropdown>
        </div>

        <div style={styles.slider}>
          <Range
            trackStyle={[
              {
                backgroundColor: colors.primary,
              },
            ]}
            handleStyle={[
              {
                borderColor: colors.primary,
              },
            ]}
            tipFormatter={value => `${value.toLocaleString('SR-rs')} €`}
            max={this.state.maxRange}
            step={this.state.rangeStep}
            defaultValue={[0, this.state.maxRange]}
            onAfterChange={(range) => {
              changePrice('min', range[0]);
              changePrice('max', range[1]);
            }}
          />
        </div>
        <div style={styles.loadMore}>
          <i
            onClick={() => this.extendMaxRange()}
            style={styles.loadMore.icon}
            className="icon_plus"
            title="Extend slider range"
          />
        </div>
      </div>
    );
  }
}

const debouncedFetchFlatList = _.debounce((dispatch) => {
  dispatch(resetFlatList());
  dispatch(fetchListOfFlats());
}, 1000);

const mapStateToProps = ({ globalSearch }) => ({
  field: globalSearch.sortField,
  direction: globalSearch.sortDirection,
  searchToolsVisible: !!globalSearch.selectedKeywords.length,
  leaseType: globalSearch.leaseType,
  numberOfRooms: globalSearch.numberOfRooms,
});
const mapDispatchToProps = dispatch => ({
  setNumberOfRooms: (numberOfRooms) => {
    dispatch(setNumberOfRooms(numberOfRooms));
    dispatch(resetFlatList());
    dispatch(fetchListOfFlats());
  },
  changeSort: (field, direction) => {
    dispatch(setSortField(field));
    dispatch(setSortDirection(direction));
    dispatch(resetFlatList());
    dispatch(fetchListOfFlats());
  },
  changePrice: (type, price) => {
    type === 'min' ? dispatch(setMinPrice(price)) : dispatch(setMaxPrice(price));

    debouncedFetchFlatList(dispatch);
  },
});

const SearchTools = connect(
  mapStateToProps,
  mapDispatchToProps,
)(Main);

export { SearchTools };
