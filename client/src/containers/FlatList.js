import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { FlatCard } from '../components';
import DefaultImage from '../assets/flat_blured.jpg';
import { loadMoreFlats } from '../redux/actions';
import { RoundedButton } from '../components/Buttons';

const styles = {
  container: {
    display: 'grid',
    gridTemplateColumns: 'repeat(auto-fit, minmax(350px, 1fr))',
    marginTop: 100,
    gridGap: 30,
    maxWidth: '80%',
    marginLeft: 'auto',
    marginRight: 'auto',
    minHeight: 41.5,
  },
  loadMore: {
    container: {
      marginTop: 100,
      marginBottom: 100,
      height: 60,
      width: '100%',
      textAlign: 'center',
    },
  },
  loading: {
    container: {
      marginLeft: 'auto',
      marginRight: 'auto',
      width: 30,
      textAlign: 'center',
      marginTop: '10%',
    },
    icon: {
      fontSize: 15,
      display: 'block',
    },
  },
};

const Main = ({
  flats,
  leaseType,
  fetchingFlats,
  loadMoreButtonVisible,
  onLoadMoreButtonClick,
}) => {
  if (fetchingFlats) {
    return (
      <div style={styles.loading.container}>
        <i style={styles.loading.icon} aria-hidden="true" className="spin icon_loading" />
      </div>
    );
  }
  if (flats.length === 0) {
    return '';
  }
  return (
    <Fragment>
      <section style={styles.container}>
        {flats.map((flat, index) => (
          <FlatCard
            key={`${index}_rent`}
            image={flat.images[0].url}
            altImage={DefaultImage}
            link={flat.link}
            price={flat.price}
            title={flat.title}
            description={flat.shortDescription}
            squareMeters={flat.squareMeters}
            numberOfRooms={flat.numberOfRooms}
            leaseType={leaseType}
          />
        ))}
      </section>
      <div style={styles.loadMore.container}>
        {loadMoreButtonVisible && (
          <RoundedButton onClick={onLoadMoreButtonClick}>Load More</RoundedButton>
        )}
      </div>
    </Fragment>
  );
};
Main.propTypes = {
  flats: PropTypes.instanceOf(Array).isRequired,
  leaseType: PropTypes.string.isRequired,
  fetchingFlats: PropTypes.bool.isRequired,
  loadMoreButtonVisible: PropTypes.bool.isRequired,
  onLoadMoreButtonClick: PropTypes.func.isRequired,
};

const mapStateToProps = ({ flatList, globalSearch }) => ({
  fetchingFlats: flatList.fetchingFlats,
  flats: flatList.list,
  leaseType: globalSearch.leaseType,
  loadMoreButtonVisible: flatList.list.length === (flatList.currentPage + 1) * flatList.perPage,
});

const mapDispatchToProps = dispatch => ({
  onLoadMoreButtonClick: () => dispatch(loadMoreFlats()),
});

const FlatList = connect(
  mapStateToProps,
  mapDispatchToProps,
)(Main);

export { FlatList };
