import React, { Component } from 'react';
import { colors } from '../style/settings';

class BackToTop extends Component {
  state = {
    scrollTop: 0,
    scrollThreshold: 2000,
    timeoutThreshold: 100,
  };
  componentDidMount() {
    window.addEventListener('scroll', this.handleScroll);
  }
  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }
  timeout = null;
  handleScroll = () => {
    clearTimeout(this.timeout);

    this.timeout = setTimeout(() => {
      this.setState({ scrollTop: document.documentElement.scrollTop });
    }, this.state.timeoutThreshold);
  };
  backToTop = () => {
    this.setState({ scrollTop: 0 });
    window.scrollTo(0, 0);
  };

  render() {
    return (
      this.state.scrollTop > this.state.scrollThreshold && (
        <a
          role="button"
          tabIndex="0"
          onKeyPress={this.backToTop}
          onClick={this.backToTop}
          style={{
            cursor: 'pointer',
            position: 'fixed',
            right: 50,
            bottom: 50,
          }}
          title="Back to top"
        >
          <i
            style={{
              color: colors.grayLight,
              fontSize: 25,
            }}
            className="arrow_carrot-2up"
          />
        </a>
      )
    );
  }
}

export { BackToTop };
