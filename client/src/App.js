import React, { Component, Fragment, StrictMode } from 'react';
import { Header, Footer, ContentWrap } from './components';
import { Homepage } from './pages';
import { DeveloperToolbar, BackToTop } from './containers';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';

const inDevelopment = process.env.NODE_ENV !== 'production';

const Search = () => (
    <ContentWrap>
        <Header />
        <Homepage />
    </ContentWrap>
);

class App extends Component {
    render() {
        return (
            <StrictMode>
                <BrowserRouter>
                    <Fragment>
                        <Switch>
                            <Route path="/search" component={Search} />
                            <Redirect to="/search" />
                        </Switch>
                        <Footer />
                        <BackToTop />
                        {inDevelopment && <DeveloperToolbar />}
                    </Fragment>
                </BrowserRouter>
            </StrictMode>
        );
    }
}

export default App;
