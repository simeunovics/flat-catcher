const config = require('../services/config');

module.exports = (req, res, next) => {
  if (
    req.get('Authorization') !==
        `Bearer ${config.getParameter('AUTH_PREDEFINED_TOKEN')}`
  ) {
    return res.status(403).send({
      error: 'Authentication required!',
    });
  }

  next();
};
