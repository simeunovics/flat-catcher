const Batch = require('../services/batchVersion');

const public = app => {
    app.get('/api/versions', async (req, res) => {
        const rentBatchId = await Batch.getLatestBatchId('rent');
        const sellBatchId = await Batch.getLatestBatchId('sell');

        res.json({ rentBatchId, sellBatchId });
    });
};

module.exports = public;
