const axios = require('axios');
const requireAuth = require('../middleware/requireAuth');
const FlatRank = require('../services/FlatRank');

const mongoose = require('mongoose');

const FlatsForSell = mongoose.model('flats_for_sell');
const FlatsForRent = mongoose.model('flats_for_rent');
const CrawlReport = mongoose.model('crawl_report');
const HalloOglasiCrawler = require('../services/HalloOglasiCrawler');
const elastic = require('../services/elastic');
const Batch = require('../services/batchVersion');

const JSDOM = require('jsdom').JSDOM;

const ESTIMATED_NUMBER_OF_PAGES_FLATS_FOR_RENT = 350;
const ESTIMATED_NUMBER_OF_PAGES_FLATS_FOR_SELL = 3000;
const TIMEOUT_BETWEEN_REQUESTS = 5000; // milliseconds
const NUMBER_OF_RETRIES = 3;
const NUMBER_OF_RETRIES_ELASTIC = 10;
const KEEP_ALIVE_TIMEOUT = 100;
const APP_URL = 'https://flat-catcher.herokuapp.com';
const NUMBER_OF_KEPT_BATCHES = 2;
const storedElasticRecords = new Set();

const webhooks = (app) => {
  app.get(
    '/api/webhook/crawl/halo-oglasi/rent',
    requireAuth,
    async (req, res) => {
      const indexName = generateIndexName('rent');
      const batchId = generateBatchId();
      await createIndexIfNotExist(indexName);
      const report = new CrawlReport({ type: 'rent', batchId });
      await report.save(() =>
        console.info(`Saved new Crawling report ${batchId}`));

      processPage(
        'https://www.halooglasi.com/nekretnine/izdavanje-stanova',
        ESTIMATED_NUMBER_OF_PAGES_FLATS_FOR_RENT,
        'rent',
        indexName,
        batchId,
        report,
      );

      res.json({ status: 'Scheduled', batchId });
    },
  );

  app.get(
    '/api/webhook/crawl/halo-oglasi/sell',
    requireAuth,
    async (req, res) => {
      const indexName = generateIndexName('sell');
      const batchId = generateBatchId();
      await createIndexIfNotExist(indexName);
      const report = new CrawlReport({ type: 'sell', batchId });
      await report.save(() =>
        console.info(`Saved new Crawling report ${batchId}`));

      processPage(
        'https://www.halooglasi.com/nekretnine/prodaja-stanova',
        ESTIMATED_NUMBER_OF_PAGES_FLATS_FOR_SELL,
        'purchase',
        indexName,
        batchId,
        report,
      );

      res.json({ status: 'Scheduled', batchId });
    },
  );
};

const processPage = async (
  url,
  max,
  leaseType,
  indexName,
  batchId,
  report,
  page = 1,
  retries = NUMBER_OF_RETRIES,
) => {
  const DBModel = leaseType === 'rent' ? FlatsForRent : FlatsForSell;

  if (page > max) {
    return await cleanUp(leaseType, DBModel, report);
  }
  console.info('Fetching ', `${url}?page=${page}`);
  if (page % KEEP_ALIVE_TIMEOUT == 0) {
    axios.get(APP_URL).then(res => console.info('Keepalive ping'));
  }

  try {
    const { data } = await axios.get(`${url}?page=${page}`, {
      timeout: 1000 * 60 * 10,
    });
    const { document } = new JSDOM(data).window;
    const records = document.querySelectorAll('.product-item.real-estates');

    records.forEach(async (domNode) => {
      const flat = new HalloOglasiCrawler(domNode, batchId).parse();
      const { extId } = flat;
      const rank = FlatRank.calculateScore(flat);

      await DBModel.findOneAndUpdate(
        { extId },
        { ...flat, rank },
        { upsert: true },
        () => console.info(`Updated ${extId}`),
      );

      if (!storedElasticRecords.has(flat.placesAsStringOptimized)) {
        storedElasticRecords.add(flat.placesAsStringOptimized);
        await sendToElastic(indexName, flat, NUMBER_OF_RETRIES_ELASTIC);
      }
    });
  } catch (e) {
    console.error(
      `\n\n Failed \t URL: [${url}] \t Page: [${page}] \t Retry: [${retries}] \t Type: [${leaseType}] \n\n`,
      e,
    );
    return (
      retries > 0 &&
            processPage(
              url,
              max,
              leaseType,
              indexName,
              batchId,
              report,
              page,
              --retries,
            )
    );
  }

  console.info(`Waiting ${TIMEOUT_BETWEEN_REQUESTS /
            1000}sec before issuing next request`);

  setTimeout(() => {
    processPage(url, max, leaseType, indexName, batchId, report, page + 1);
  }, TIMEOUT_BETWEEN_REQUESTS);
};

const cleanUp = async (leaseType, DBModel, report) => {
  const batches = await CrawlReport.find({ type: leaseType }).sort({
    createdAt: -1,
  });

  console.info('Batches to delete', batches.slice(NUMBER_OF_KEPT_BATCHES));

  batches.slice(NUMBER_OF_KEPT_BATCHES).map(async ({ batchId }) => {
    await DBModel.deleteMany({ 'meta.batchId': batchId, type: leaseType });
    console.info(`Batch ${batchId} cleared.`);

    await elastic.deleteBatch(leaseType, batchId);
    console.info(`Records type: ${leaseType}, batchId: ${batchId} cleared`);
  });
  await report.update({ completed: true });
  await CrawlReport.deleteMany({ type: leaseType, completed: false });
  console.info('Cleaning done');
};

const sendToElastic = async (indexName, flat, numOfRetries) => {
  try {
    if (!numOfRetries) {
      return console.error('Unable to index flat. Timeout exceeded', {
        index: `${indexName}`,
        flat,
        numOfRetries,
      });
    }

    await elastic.index.addFlat(indexName, flat);
  } catch (e) {
    console.error('Unable to index flat', {
      index: `${indexName}`,
      flat,
      exception: e,
      numOfRetries,
    });
    setTimeout(
      () => sendToElastic(indexName, flat, --numOfRetries),
      TIMEOUT_BETWEEN_REQUESTS,
    );
  }
};

const generateIndexName = type => type;

const generateBatchId = () => Date.now();

const createIndexIfNotExist = async (indexName) => {
  try {
    const indexExists = await elastic.index.exists(indexName);
    if (!indexExists) {
      const index = await elastic.index.create(indexName);
    }
  } catch (e) {
    console.error(`Unable to create index "${indexName}"`, e);
  }
};

module.exports = webhooks;
