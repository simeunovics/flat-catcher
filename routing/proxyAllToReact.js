const proxyEverythingToReact = (app) => {
  app.get('*', (req, res) => {
    res.sendFile(require('path').resolve(__dirname, 'client', 'build', 'index.html'));
  });
};

module.exports = proxyEverythingToReact;
