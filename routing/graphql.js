const mongoose = require('mongoose');

const FlatsForRent = mongoose.model('flats_for_rent');
const FlatsForSell = mongoose.model('flats_for_sell');
const graphqlHTTP = require('express-graphql');
const { buildSchema } = require('graphql');
const elastic = require('../services/elastic');
const Config = require('../services/config');

const escapeRegex = text => text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&');
const buildFlatSearchQuery = (request) => {
  const {
    locations, minPrice, maxPrice, numberOfRooms, maxMeters, minMeters, batchId,
  } = request;
  let query = {
    placesAsString: {
      $regex: new RegExp(escapeRegex(locations), 'gi'),
    },
  };

  if (batchId) {
    query = {
      ...query,
      'meta.batchId': batchId,
    };
  }

  if (minPrice || maxPrice) {
    query = {
      ...query,
      price: { $gte: minPrice || 0, $lte: maxPrice || 99999999 },
    };
  }

  if (minMeters || maxMeters) {
    query = {
      ...query,
      squareMeters: {
        $gte: minMeters || 0,
        $lte: maxMeters || 99999999,
      },
    };
  }

  if (numberOfRooms) {
    query = {
      ...query,
      numberOfRooms:
        numberOfRooms > 5
          ? { $gte: numberOfRooms }
          : { $gte: numberOfRooms, $lt: numberOfRooms + 1 },
    };
  }

  return query;
};

const schema = buildSchema(`
    type Image {
        url: String
    }
    type Flat {
        extId: String
        title: String
        leaseType: String
        link: String
        price: String
        shortDescription: String
        squareMeters: String
        numberOfRooms: String
        images: [Image]
        places: [Place]
    }
    type AutocompleteResult {
        text: String
    }
    type Place {
        text: String
    }
    type Sort {
        field: String
        direction: String
    }

    type Query {
        flatsForRent(
            skip: Int=0
            limit: Int=100
            locations: String!
            sortField: String="createdAt"
            "'desc' or 'asc'"
            sortDirection: String="desc"
            minPrice: Int
            maxPrice: Int
            numberOfRooms: Int
            maxMeters: Int
            minMeters: Int
            batchId: String
        ): [Flat]
        flatsForSell(
            skip: Int=0
            limit: Int=100
            locations: String!
            sortField: String="createdAt"
            "'desc' or 'asc'"
            sortDirection: String="desc"
            minPrice: Int
            maxPrice: Int
            numberOfRooms: Int
            maxMeters: Int
            minMeters: Int
            batchId: String
        ): [Flat]
        locationAutocomplete(
            query: String!
            "Rent or Sell"
            leaseType: String!
            batchId: String
        ): [AutocompleteResult]
    }
`);

const findFlats = (model, request) => {
  const {
    limit, skip, sortField, sortDirection,
  } = request;
  return model
    .find(buildFlatSearchQuery(request))
    .limit(limit)
    .skip(skip)
    .sort({
      [sortField]: sortDirection === 'desc' ? -1 : 1,
    });
};

const root = {
  flatsForSell: (...args) => findFlats(FlatsForSell, ...args),
  flatsForRent: (...args) => findFlats(FlatsForRent, ...args),
  locationAutocomplete: async ({ query, leaseType, batchId }) => {
    if (Config.isOn('FEATURE_ELASTIC_SEARCH_SUGGESTIONS')) {
      const { hits } = await elastic.search(leaseType.toLowerCase(), 'flat', batchId, query);

      return hits.hits.map(hit => ({
        text: hit._source.placesAsString,
      }));
    }

    const Flats = leaseType.toLowerCase() === 'sell' ? FlatsForSell : FlatsForRent;

    return Flats.aggregate([
      {
        $match: {
          placesAsStringOptimized: {
            $regex: new RegExp(escapeRegex(query), 'gi'),
          },
          'meta.batchId': batchId,
        },
      },
      {
        $project: { placesAsString: true },
      },
      {
        $group: {
          _id: '$placesAsString',
          places: { $addToSet: '$placesAsString' },
          count: { $sum: 1 },
        },
      },
      {
        $project: {
          text: '$_id',
          count: 1,
          places: 1,
        },
      },
      { $sort: { count: -1 } },
    ]).limit(10);
  },
};

const graphql = (app) => {
  app.use(
    '/api/explorer',
    graphqlHTTP({
      schema,
      rootValue: root,
      graphiql: true,
    }),
  );
};

module.exports = graphql;
