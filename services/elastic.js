const config = require('./config');
const elasticsearch = require('elasticsearch');
const logging = require('./logging');

class Index {
  constructor(connection) {
    this.logger = logging.getInstance();
    this.connection = connection;
  }

  async exists(options) {
    if (typeof options === 'string') {
      await this.connection.indices.exists({ index: options });
    }

    await this.connection.indices.exists(options);
  }

  async create(options) {
    if (typeof options === 'string') {
      await this.connection.indices.create({ index: options });
    }

    await this.connection.indices.create(options);
  }

  async addFlat(index, {
    placesAsString, placesAsStringOptimized, type, meta,
  }) {
    const body = {
      placesAsString,
      placesAsStringOptimized,
      batchId: meta.batchId,
    };

    await this.connection.index({ index, type, body });
  }
}

class Elastic {
  constructor(host) {
    this.connection = new elasticsearch.Client({ host });
    this.index = new Index(this.connection);
  }

  async deleteBatch(index, batchId) {
    try {
      return await this.connection.deleteByQuery({
        index,
        body: {
          query: {
            term: { batchId },
          },
        },
      });
    } catch (e) {
      this.logger.error(e);

      return undefined;
    }
  }

  async search(index, type, batchId, userQuery) {
    await this.connection.search({
      index,
      type,
      body: {
        query: {
          bool: {
            must: [
              {
                wildcard: {
                  placesAsStringOptimized: `*${userQuery.toLowerCase()}*`,
                },
              },
              {
                match: { batchId },
              },
            ],
          },
        },
        size: 10,
      },
    });
  }
}

module.exports = ((...args) => new Elastic(...args))(config.getParameter('ELASTIC_SEARCH_CONNECTION_URI'));
