exports.canonicalLatin = (string) => {
  const latinMap = {
    đ: 'dj',
    š: 's',
    ž: 'z',
    ć: 'c',
    č: 'c',
  };
  const search = Object.keys(latinMap).join('|');
  return string.replace(new RegExp(search, 'gi'), letter => latinMap[letter] || letter);
};

exports.strintToInt = string =>
  parseInt(
    string
      .trim()
      .replace(new RegExp('\\.', 'g'), '')
      .replace(new RegExp(',', 'g'), ''),
    10,
  );

exports.stringToFloat = (string) => {
  const text = string.trim();

  if (!text.includes(',')) {
    return parseFloat(text);
  }

  if (text.indexOf(',') < text.indexOf('.')) {
    return parseFloat(text.replace(new RegExp(',', 'g'), ''));
  }

  return parseFloat(text.replace(new RegExp('\\.', 'g'), '').replace(new RegExp(',', 'g'), '.'));
};
