const mongoose = require('mongoose');

const CrawlReport = mongoose.model('crawl_report');

exports.getLatestBatchId = async (type) => {
  try {
    const { batchId } = await CrawlReport.findOne({
      completed: true,
      type,
    }).sort({
      createdAt: -1,
    });

    return batchId;
  } catch (e) {
    console.error(e);
    return undefined;
  }
};
