const Raven = require('raven');
const config = require('./config');

module.exports = (app) => {
  Raven.config(config.getParameter('SENTRY_DSN')).install();
  // The request handler must be the first middleware on the app
  app.use(Raven.requestHandler());
  // The error handler must be before any other error middleware
  app.use(Raven.errorHandler());
};
