class Config {
  constructor(parameters = {}) {
    this.parameters = parameters;
  }

  getParameter(parameter) {
    return parameter in this.parameters ? this.parameters[parameter] : undefined;
  }

  isProductionEnv() {
    return this.getParameter('NODE_ENV') === 'production';
  }

  isOn(feature) {
    return this.getParameter(feature) === 'true';
  }
}

module.exports = new Config({ ...process.env });
