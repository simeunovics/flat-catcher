const moment = require('moment');
const stringTransformer = require('./stringTransformer');

const issuerTypes = {
  agencija: 'agency',
  vlasnik: 'owner',
  investitor: 'investor',
};
const immobiliareTypes = {
  stan: 'flat',
};

class HalloOglasiCrawler {
  constructor(domNode, batchId, baseUrl = 'https://www.halooglasi.com') {
    this.NO_ROOMS_UNKNOWN_MAX = 15.15;
    this.baseUrl = baseUrl;
    this.domNode = domNode;
    this.batchId = batchId;
    this.stringTransformer = stringTransformer;
  }

  parse() {
    return {
      extId: this.getLink(),
      title: this.getTitle(),
      link: this.getLink(),
      price: this.getPrice(),
      shortDescription: this.getShortDescription(),
      squareMeters: this.getSquareMeters(),
      numberOfRooms: this.getNumberOfRooms(),
      placesAsStringOptimized: this.getPlaces()
        .map(({ text }) => text.toLowerCase())
        .map(text => this.stringTransformer.canonicalLatin(text))
        .join(' '),
      placesAsString: this.getPlaces()
        .map(({ text }) => text)
        .join(' / '),
      type: this.getType(),
      issuer: this.getIssuer(),
      publishedAt: this.getPublishedAt(),
      meta: { batchId: this.batchId },
      provider: {
        name: 'HaloOglasi',
        link: this.baseUrl,
      },
      images: [
        {
          url: this.getImage(),
        },
      ],
    };
  }

  getTitle() {
    return this.extractValue(() => this.domNode.querySelector('.ad-title > a').textContent.trim());
  }

  getLink() {
    return this.extractValue(() => this.baseUrl + this.domNode.querySelector('.ad-title > a').attributes.href.value.trim());
  }

  getPrice() {
    return this.extractValue(() =>
      this.stringTransformer.stringToInt(this.domNode.querySelector('.central-feature > span').attributes['data-value'].value));
  }

  getShortDescription() {
    return this.extractValue(() =>
      this.domNode.querySelector('.ad-description.short-desc').textContent.trim());
  }

  getType() {
    const type = this.extractValue(() =>
      this.domNode
        .querySelector('.ad-features li:nth-child(1) .value-wrapper')
        .textContent.trim()
        .split('\xA0')[0]).trim();

    return type.toLowerCase() in immobiliareTypes ? immobiliareTypes[type.toLowerCase()] : type;
  }

  getSquareMeters() {
    return this.extractValue(() =>
      this.stringTransformer.stringToFloat(this.domNode
        .querySelector('.ad-features li:nth-child(2) .value-wrapper')
        .textContent.match(/\d+/)[0]));
  }

  getNumberOfRooms() {
    const noRooms = this.extractValue(() =>
      this.domNode
        .querySelector('.ad-features li:nth-child(3) .value-wrapper')
        .textContent.trim()
        .split('\xA0')[0]);

    return noRooms.includes('+')
      ? this.NO_ROOMS_UNKNOWN_MAX
      : this.stringTransformer.stringToFloat(noRooms);
  }

  getImage() {
    return this.extractValue(() =>
      this.domNode
        .querySelector('.a-images > img')
        .attributes.src.value.replace('/m/', '/l/')
        .trim());
  }

  getPlaces() {
    return this.extractValue(() => {
      const values = [];

      this.domNode
        .querySelectorAll('.subtitle-places li')
        .forEach(elem => values.push({ text: elem.textContent.trim() }));

      return values.filter(({ text }) => text.length);
    });
  }

  getIssuer() {
    const issuer = this.extractValue(() =>
      this.domNode
        .querySelector('.pi-img-wrapper-under .basic-info [data-field-name="oglasivac_nekretnine_s"]')
        .textContent.trim());

    return issuer.toLowerCase() in issuerTypes ? issuerTypes[issuer.toLowerCase()] : issuer;
  }

  getPublishedAt() {
    const date = this.extractValue(() =>
      this.domNode.querySelector('.pi-img-wrapper-under .publish-date').textContent.trim());

    return moment(date, 'DD.MM.YYYY').toDate();
  }

  extractValue(callback) {
    try {
      return callback.call(this);
    } catch (e) {
      return '';
    }
  }
}

module.exports = HalloOglasiCrawler;
