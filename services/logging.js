const winston = require('winston');
const config = require('./config');

const defaultConfig = {
  level: 'info',
  format: winston.format.json(),
  transports: [
    //
    // - Write to all logs with level `info` and below to `combined.log`
    // - Write all logs error (and below) to `error.log`.
    //
    new winston.transports.File({ filename: 'error.log', level: 'error' }),
    new winston.transports.File({ filename: 'combined.log' }),
  ],
};

const getInstance = (options = {}) => {
  const logger = winston.createLogger({
    ...defaultConfig,
    ...options,
  });

  if (!config.isProductionEnv()) {
    return logger;
  }

  logger.add(new winston.transports.Console({
    format: winston.format.simple(),
  }));

  return logger;
};

module.exports = { getInstance };
