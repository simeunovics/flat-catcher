// Initialize configuration
require('dotenv').config();

const express = require('express');
const config = require('./services/config');

// Mongo
const mongoose = require('mongoose');
require('./model/Flat');
require('./model/Crawl');
mongoose.connect(config.getParameter('DB_URI'));

// App
const app = express();

// Sentry
if (config.isProductionEnv()) {
    require('./services/sentry')(app);
}

// Routing
require('./routing/webhooks')(app);
require('./routing/graphql')(app);
require('./routing/public')(app);
if (config.isProductionEnv()) {
    app.use(express.static('./client/build'));
    require('./routing/proxyAllToReact')(app);
}

const PORT = config.getParameter('PORT') || 5000;
app.listen(PORT);
