const mongoose = require('mongoose');

const { Schema } = mongoose;

const flatSchema = new Schema({
  batchId: String,
  type: String,
  completed: { type: Boolean, default: false },
  createdAt: { type: Date, default: Date.now },
});

mongoose.model('crawl_report', flatSchema);
