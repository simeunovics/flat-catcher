const mongoose = require('mongoose');

const { Schema } = mongoose;

const flatSchema = new Schema({
  extId: String,
  title: String,
  link: String,
  price: Number,
  images: [
    {
      url: String,
    },
  ],
  places: [
    {
      text: String,
    },
  ],
  placesAsString: String,
  placesAsStringOptimized: String,
  shortDescription: String,
  squareMeters: Number,
  numberOfRooms: Number,
  type: String,
  issuer: String,
  publishedAt: { type: Date },
  createdAt: { type: Date, default: Date.now },
  provider: {
    name: String,
    link: String,
  },
  meta: {
    batchId: String,
  },
  rank: { type: Number, default: -1 },
});

mongoose.model('flats_for_sell', flatSchema);
mongoose.model('flats_for_rent', flatSchema);
